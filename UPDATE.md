# How to update the Scaffolding Project to a new version of COYO

## Backend

### Step 1: 3rd-party-library updates

Check versions of dependencies in the following files:

- build.gradle

## Step 2: Compare various files

- COYO-SCAFFOLDING/backend/docker-entrypoint.sh
- COYO-SCAFFOLDING/backend/.docker-ignore
- ...

### Step 3: COYO library update

Change the version of variables of type "x.y.z-BETA/RELEASE" in the following files:

- COYO-SCAFFOLDING/backend/build.gradle
- COYO-SCAFFOLDING/tools/dev/.env
- COYO-SCAFFOLDING/tools/run/.env

Check for new variables/environment variables in the following files compared to the file COYO/run/.env:

- COYO-SCAFFOLDING/tools/dev/.env
- COYO-SCAFFOLDING/tools/run/.env

## Step 4: Check docker setup

Compare the environment variables and containers in the following files compared to the file COYO/tools/dev/docker-compose.dev.yml:

- COYO-SCAFFOLDING/tools/dev/docker-compose.dev.yml
- COYO-SCAFFOLDING/tools/dev/docker-compose.dev.backend.yml
- COYO-SCAFFOLDING/tools/dev/docker-compose.dev.frontend.yml

Compare the environment variables and containers in the following files compared to the file COYO/run/docker-compose.tmpl:

- COYO-SCAFFOLDING/tools/run/docker-compose.yml

## Step 5: Gradle refresh

- Refresh the gradle dependencies

## Frontend

### Step 1: 3rd-party-library updates

- Copy the bower.json overrides section from "COYO/coyo-frontend/bower.json"
- Paste it to the overrides section of "COYO-SCAFFOLDING/frontend/gulp/conf.js"

## Step 2: Compare various files

- COYO-SCAFFOLDING/frontend/docker-entrypoint.sh
- COYO-SCAFFOLDING/frontend/.docker-ignore
- COYO-SCAFFOLDING/frontend/protractor.conf.js
- COYO-SCAFFOLDING/frontend/assembly/httpd.conf
- COYO-SCAFFOLDING/frontend/assembly/config.js
- COYO-SCAFFOLDING/frontend/gulp/.eslintrc
- ...

### Step 3: COYO library update

Change the version of variables of type "x.y.z-BETA/RELEASE" in the following files:

- COYO-SCAFFOLDING/frontend/bower.json

## Step 4: Frontend resolutions update

- Delete the resolutions section of "COYO-SCAFFOLDING/frontend/bower.json" and run "npm install".
- Afterwards, remove all relative version identifiers, e.g. "~", and compare versions with the versions in "COYO/coyo-frontend/bower.json".

## Step 5: Check for missing images/files

Check whether there are new files in the following folder:

- COYO/coyo-frontend/src/main/assets

Copy over these new files to the following folder:

- COYO-SCAFFOLDING/frontend/src/assets
