import {ElementFinder, protractor} from 'protractor';
import {BaseFragment} from 'protractor-element-extend';
import {promise as wdpromise} from 'selenium-webdriver';
import * as _ from 'underscore';
import {applyMixins} from '../../../TypescriptUtil';
import {Action, ActionHolder, Actions, HasActions} from './Action';

export class Form<C> implements HasActions<C> {

    // inherited from HasActions
    public actions: ActionHolder = new ActionHolder();
    public isActionSelectable: (action: Actions | string) => wdpromise.Promise<boolean>;
    public setActions: (actions?: Action[]) => C;
    public addAction: (actionGenerator: (component: C) => Action) => C;
    public selectAction: (action: Actions | string) => C;

    public fillForm(data: any | any[]): C {
        const getData = (property: string) => data[property]; // FIXME implement using data[] too
        _.keys(this)
            .map((key) => this[key])
            .filter((property: any) => property && property.type)
            .forEach((formElement: FormElement<any>) => {
                formElement.setValue(getData(formElement.name));
            });
        return this as any as C;
    }

    public doSubmit(): C {
        this.selectAction(Actions.Submit);
        return this as any as C;
    }
}

applyMixins(Form, [HasActions]);

export abstract class FormElement<C> extends BaseFragment {

    public type = 'formElement';
    public name: string;

    constructor(name: string, elementFinder: ElementFinder) {
        super(elementFinder);
        this.name = name;
    }

    public abstract setValue(value: string | number | boolean): C;
}

export class TextField extends FormElement<TextField> {

    public type = 'text';

    public getValue(): wdpromise.Promise<string> {
        return null; // FIXME implement
    }

    public setValue(value: string | number | boolean): TextField {
        this.clear().then(() => this.sendKeys(String(value)));
        return this;
    }
}

export class Checkbox extends FormElement<Checkbox> {

    public type = 'checkbox';
    public isChecked: () => wdpromise.Promise<boolean>;
    private elemToggle: ElementFinder;

    constructor(name: string, elementFinder: ElementFinder, clickable: ElementFinder,
                isChecked: () => wdpromise.Promise<boolean>) {
        super(name, elementFinder);
        this.elemToggle = clickable;
        this.isChecked = isChecked;
    }

    public toggle(): Checkbox {
        this.elemToggle.click();
        return this;
    }

    public set(checked?: boolean): Checkbox {
        const value = _.isUndefined(checked) ? true : checked;
        this.isChecked().then((isChecked) => {
            return isChecked !== value ? this.toggle() : protractor.promise.fulfilled();
        });
        return this;
    }

    public setValue(value: string | number | boolean): Checkbox {
        return this.set(!!value);
    }

    public reset(): Checkbox {
        this.set(false);
        return this;
    }
}

export class ContextMenu extends BaseFragment implements HasActions<ContextMenu> {

    public dropdown: ElementFinder = this.$('.dropdown-menu');
    // inherited from HasActions
    public actions: ActionHolder = new ActionHolder();
    public isActionSelectable: (action: Actions | string) => wdpromise.Promise<boolean>;
    public setActions: (actions?: Action[]) => ContextMenu;
    public addAction: (actionGenerator: (component: ContextMenu) => Action) => ContextMenu;
    public selectAction: (action: Actions | string) => ContextMenu;

    constructor(elementFinder: ElementFinder, actions?: Action[]) {
        super(elementFinder); // FIXME use this.$(‘.context-menu.dropdown')
        this.setActions(actions);
    }

    public open(): ContextMenu {
        this.isOpen().then((isOpen) => isOpen ? protractor.promise.fulfilled<void>() : this.click());
        return this;
    }

    public isOpen(): wdpromise.Promise<boolean> {
        return this.dropdown.isDisplayed();
    }

    /*
        isActionSelectable(action: Actions | string): wdpromise.Promise<boolean> {
            let act = this.actions.find(action);
            return this.Present()
                .then((isPresent) => true); // FIXME
        }
    */

    protected _selectAction(fnSuper: (action: Actions | string) => ContextMenu, action: Actions | string): ContextMenu {
        this.open();
        fnSuper(action);
        return this;
    }
}

applyMixins(ContextMenu, [HasActions]);

export class Select extends FormElement<Select> {

    public type = 'select';

    // TODO implement


    public setValue(value: string | number | boolean): Select {
        // TODO implement
        return this;
    }
}

export class Dialog<C> extends BaseFragment implements Form<C>, HasActions<C> {

    // inherited from HasActions
    public actions: ActionHolder = new ActionHolder();
    public isActionSelectable: (action: Actions | string) => wdpromise.Promise<boolean>;
    public setActions: (actions?: Action[]) => C;
    public addAction: (actionGenerator: (component: C) => Action) => C;
    public selectAction: (action: Actions | string) => C;
    // inherited from Form
    public fillForm: (data: any | any[]) => C;
    public doSubmit: () => C;

    constructor(elementFinder: ElementFinder, actions?: Action[]) {
        super(elementFinder);
        this.setActions(actions);
    }

    public close(action: Actions | string): C {
        this.selectAction(action);
        return this as any as C;
    }

    public dismiss(): C {
        return this.close(Actions.Cancel);
    }
}

applyMixins(Dialog, [HasActions]);
