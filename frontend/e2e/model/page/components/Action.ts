import {promise as wdpromise} from 'selenium-webdriver';
import * as _ from 'underscore';
import {ElementFinder, protractor} from 'protractor';
import {BaseFragment} from 'protractor-element-extend';

export enum Actions {
    Submit = 'Submit',
    Cancel = 'Cancel',
    Delete = 'Delete',
    Edit = 'Edit',
}

export class Action extends BaseFragment {

    public static submitButton(parent: ElementFinder): Action {
        return new Action(Actions.Submit, parent.$('button[type="submit"]'));
    }

    public name: string;

    constructor(name: string | Actions, elementFinder: ElementFinder) {
        super(elementFinder);
        this.name = name;
    }

    public select(): wdpromise.Promise<any> {
        return this.click();
    }

    public isSelectable(): wdpromise.Promise<boolean> {
        return this.isDisplayed();
    }
}

export class ActionHolder {
    private actions: Action[];

    constructor(actions?: Action[]) {
        this.actions = actions || [];
    }

    public add(action: Action): void {
        this.actions.push(action);
    }

    public find(action: Actions | string): Action | undefined {
        return this.actions.find((item) => item.name === action);
    }

    public findOrFail(action: Actions | string): Action {
        const act = this.find(action);
        if (_.isUndefined(act)) {
            throw new Error('No action "' + action + '" found');
        }
        return act;
    }
}

export class HasActions<C> implements HasActions<C> {

    public actions: ActionHolder;

    public isActionSelectable(action: Actions | string): wdpromise.Promise<boolean> {
        const act = this.actions.find(action);
        return _.isUndefined(act) ? protractor.promise.fulfilled(false) : act.isSelectable();
    }

    public setActions(actions?: Action[]): C {
        this.actions = new ActionHolder(actions);
        return this as any as C;
    }

    public addAction(actionGenerator: (component: C) => Action): C {
        this.actions.add(actionGenerator(this as any as C));
        return this as any as C;
    }

    public selectAction(action: Actions | string): C {
        this.actions.findOrFail(action).select();
        return this as any as C;
    }
}
