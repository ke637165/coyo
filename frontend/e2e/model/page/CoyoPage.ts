import {promise as wdpromise} from 'selenium-webdriver';
import {Page} from './Page';
import {$, ElementFinder} from 'protractor';


export class CoyoPage extends Page {

    public static addPageOpenInterceptors(interceptor: (value: any) => (page: CoyoPage) => wdpromise.IThenable<any>) {
        CoyoPage.pageOpenInterceptors.push(interceptor);
    }

    private static pageOpenInterceptors: Array<(value: any) => (page: CoyoPage) => wdpromise.IThenable<any>> = [];

    private path: string;
    private elemTourClose: ElementFinder = $('.tour-step-close');

    constructor(path: string) {
        super(path);
        this.path = path;
    }

    public open(woInterceptors?: boolean): wdpromise.Promise<any> { // FIXME return CoyoPage
        const interceptors = woInterceptors ? [] : CoyoPage.pageOpenInterceptors;
        return interceptors.reduce((promise, interceptor) => promise.then(interceptor(this)), super.open());
    }

    public isTourDisplayed(): wdpromise.Promise<boolean> {
        return this.elemTourClose.isPresent().then((isPresent) => isPresent && this.elemTourClose.isDisplayed());
    }

    public closeTour(): wdpromise.Promise<any> { // FIXME return CoyoPage
        return this.elemTourClose.click();
    }
}
