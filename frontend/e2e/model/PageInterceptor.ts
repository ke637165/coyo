import {browser, protractor} from 'protractor';
import {CoyoPage} from './page/CoyoPage';
import {ConfigPage} from './page/ConfigPage';
import {LoginPage} from './page/LoginPage';
import {User} from './data/User';
import {Condition, Log, TestHelper} from './Util';
import * as URI from 'urijs';

export class PageInterceptorBuilder {

    private interceptors = [];

    public withBackendServer(urlResolver: () => string): PageInterceptorBuilder {
        this.interceptors.push((/* page */) => () => new ConfigPage().isDisplayed());
        this.interceptors.push((/* page */) => (notConfigured) => {
            if (notConfigured) {
                if (urlResolver) {
                    return new ConfigPage()
                        .setServerUrl(urlResolver())
                        .submit()
                        .then(() => browser.sleep(500))
                        .then(() => protractor.promise.fulfilled(true));
                } else {
                    return protractor.promise.rejected(new Error('COYO can not be configured'));
                }
            } else {
                return protractor.promise.fulfilled(false);
            }
        });
        this.interceptors.push((page: CoyoPage) =>
            (deeplinkDiscarded) => deeplinkDiscarded ? page.open(true) : protractor.promise.fulfilled());
        return this;
    }

    public withUserLogin(user: User): PageInterceptorBuilder {
        this.interceptors.push((/* page */) => () => new LoginPage().isDisplayed());
        this.interceptors.push((/* page */) => (notLoggedIn) => Condition.when(notLoggedIn)
            .then(() => new LoginPage().login(user))
            .return(protractor.promise.fulfilled()));
        return this;
    }

    public withTourCanceler(): PageInterceptorBuilder {
        this.interceptors.push((page: CoyoPage) => () => page.isTourDisplayed());
        this.interceptors.push((page: CoyoPage) =>
            (tourIsDisplayed) => tourIsDisplayed ? page.closeTour() : protractor.promise.fulfilled());
        return this;
    }

    public withDisabledAnimations(): PageInterceptorBuilder {
        this.interceptors.push((/* page */) => () => TestHelper.disableAnimations());
        return this;
    }

    // noinspection JSUnusedGlobalSymbols
    public withLogMessage(msg: string): PageInterceptorBuilder {
        this.interceptors.push((/* page */) => () => {
            Log.info(msg);
            return protractor.promise.fulfilled();
        });
        return this;
    }

    public buildAndRegister(): void {
        this.interceptors.forEach((interceptor) => CoyoPage.addPageOpenInterceptors(interceptor));
    }
}

export class PageInterceptor {

    public static builder(): PageInterceptorBuilder {
        return new PageInterceptorBuilder();
    }

    public static registerDefaultInterceptors(): void {
        PageInterceptor.builder()
        // .withLogMessage('begin page interceptors')
            .withBackendServer(PageInterceptor.defaultBackendUrlResolver)
            // .withLogMessage('after backend configured')
            .withUserLogin(PageInterceptor.defaultUser())
            // .withLogMessage('after user login')
            .withTourCanceler()
            // .withLogMessage('after tour cancelled')
            .withDisabledAnimations()
            // .withLogMessage('end page interceptors')
            .buildAndRegister();
    }

    public static defaultBackendUrlResolver(): string {
        return new URI(browser.baseUrl).port(8080).toString();
    }

    public static defaultUser(): User {
        return new User('ian.bold@coyo4.com', 'demo');
    }
}
