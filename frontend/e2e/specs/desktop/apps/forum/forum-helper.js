(function () {
  'use strict';

  var App = require('./../app.page');
  var ForumPage = require('./forum.page');
  var WorkspaceDetail = require('./../../workspaces/workspace-details.page.js');
  var component = require('./../../../components.page');

  var app = new App();
  var forumPage = new ForumPage();
  var workspaceDetail = new WorkspaceDetail();

  function ForumHelper() {
    var api = this;

    api.createEmptyForumApp = createEmptyForumApp;
    api.createForumThread = createForumThread;
    api.getFirstVisibleElement = getFirstVisibleElement;
    api.hoverElement = hoverElement;

    function createEmptyForumApp() {
      var appName = 'Forum';

      // add a forum app
      workspaceDetail.options.addApp.click();
      app.app.forum.click();

      forumPage.setting.appName.clear();
      forumPage.setting.appName.sendKeys(appName);

      expect(forumPage.setting.active.isChecked()).toBeTruthy();

      component.modals.confirm.confirmButton.click();

      expect(forumPage.threadList.empty.isPresent()).toBeTruthy();
    }

    function createForumThread(title, text, attachmentPath) {

      expect(getFirstVisibleElement(forumPage.threadList.create).isPresent()).toBeTruthy();
      getFirstVisibleElement(forumPage.threadList.create).click();

      expect(forumPage.thread.create.header.isPresent()).toBeTruthy();
      expect(forumPage.thread.create.titleInput.isPresent()).toBeTruthy();
      expect(forumPage.thread.create.editorInput.isPresent()).toBeTruthy();

      forumPage.thread.create.titleInput.sendKeys(title);
      forumPage.thread.create.editorInput.sendKeys(text);

      if (attachmentPath) {
        // attachments disabled for bamboo ...
        // forumPage.thread.create.attachment.button.click();
        // forumPage.thread.create.attachment.input.sendKeys(attachmentPath);
      }
      forumPage.thread.create.saveButton.click();
    }

    function getFirstVisibleElement(selector) {
      var allElementsOfSelector = element.all(by.css(selector.locator().value));
      return allElementsOfSelector.filter (function (e) {
        return e.isDisplayed().then (function (displayedElement) {
          return displayedElement;
        });
      }).first();
    }

    function hoverElement(selector) {
      browser.actions().mouseMove(selector).perform();
    }

  }

  module.exports = ForumHelper;

})();
