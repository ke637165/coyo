(function () {
  'use strict';

  var Checkbox = require('./../../../checkbox.page');

  function ListApp() {
    var api = this;

    var container = $('.create-app-form');
    var advancedSettings = $('.form-advanced');

    api.setting = {
      ListNameNavigation: container.element(by.model('$ctrl.languages[key].translations.name')),
      active: new Checkbox(container.element(by.model('$ctrl.app.active'))),
      modalName: container.element(by.model('$ctrl.app.settings.elementName')),
      advanced: $('div[ng-click="$ctrl.showAdvanced = !$ctrl.showAdvanced"]'),
      createEntryGroup: {
        yes: advancedSettings.$('input[name="create"][value="YES"]'),
        no: advancedSettings.$('input[name="create"][value="NO"]')
      },
      seeEntryGroup: {
        none: advancedSettings.$('input[name="read"][value="NONE"]')
      }
    };

    var appContainer = $('.list-app');
    api.listAppTable = {
      emptyAppDefaultText: appContainer.$('.app-empty-text'),
      newFieldBtn: appContainer.$('button[translate="APP.COMMONS.FIELD.ADD"]'),
      newEntryBtn: appContainer.$('button[translate="APP.LIST.ADD_ENTRY_BUTTON"]'),
      search: function (term) {
        appContainer.$('input[type="search"]').clear();
        return appContainer.$('input[type="search"]').sendKeys(term);
      },
      entries: element.all(by.repeater('entry in $ctrl.currentPage.content')),
      viewDetailEntry: function (index) {
        var elem = api.listAppTable.entries.get(index);
        browser.actions().mouseMove(elem).perform();

        $('.dynamic-app-table .dropdown.dropdown-toggle').click();
        $('a[ng-click="$ctrl.viewEntry(entry)"]').click();
      }
    };

    api.entryDetailView = {
      historyLabel: $('span[translate="APP.LIST.DETAIL.MODAL.HISTORY"]'),
      detailsLabel: $('span[translate="APP.LIST.DETAIL.MODAL.ENTRY"]'),
      historyBtn: $('.modal-subheader a[ng-click="$ctrl.switchTab(\'history\')"]'),
      createdByUser: $('a[ui-sref="main.profile({userId: $ctrl.history.author.slug})"]'),
      entryDetailsList: $('.list-entry-details')
    };

    api.configureFields = {
      modalBtn: $('.context-menu-toggle'),
      configureFieldsButton: $('a[ui-sref="^.configure"]'),
      items: element.all(by.repeater('field in $ctrl.fields')),
      fieldType: function (fieldIndex) {
        return api.configureFields.items.get(fieldIndex).$('.field-type');
      },
      fieldName: function (fieldIndex) {
        return api.configureFields.items.get(fieldIndex).$('.field-name');
      },
      addFieldBtn: $('a[ng-click="$ctrl.createField()"]'),
      backBtn: $('.btn-back')
    };

    var allFieldTypes = element.all(by.repeater('type in $ctrl.fieldTypes'));
    api.fieldTypes = {
      checkbox: allFieldTypes.get(0),
      option: allFieldTypes.get(5),
      text: allFieldTypes.get(6),
      user: allFieldTypes.get(7)
    };

    api.fieldSettingsCheckbox = {
      name: element(by.model('$ctrl.field.name'))
    };

    api.fieldSettingsUser = {
      name: element(by.model('$ctrl.field.name')),
      required: new Checkbox(element(by.model('$ctrl.field.required')))
    };

    api.fieldSettingsOption = {
      name: element(by.model('$ctrl.field.name')),
      option: function (optionIndex) {
        return $('#options-field-option-' + optionIndex);
      },
      addOptionBtn: $('.add-button')
    };

    api.fieldSettingsText = {
      name: element(by.model('$ctrl.field.name')),
      required: new Checkbox(element(by.model('$ctrl.field.required')))
    };
  }

  module.exports = ListApp;

})();
