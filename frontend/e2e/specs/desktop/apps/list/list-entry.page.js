
(function () {
  'use strict';

  function ListEntry() {
    var api = this;

    var Checkbox = require('./../../../checkbox.page');

    api.chooseUser = {
      btn: $('coyo-user-chooser').$('span[ng-click="$ctrl.openChooser()"]'),
      selectUserBtn: $('button[translate="USER_CHOOSER.SAVE"]'),
      choose: function () {
        api.chooseUser.btn.click();
      },
      selectUserName: function (name) {
        element(by.cssContainingText('.item-headline', name)).click();
      }
    };

    api.noReadPermissionsLabel = $('span[translate="APP.LIST.NO_READ_PERMISSIONS"]');

    api.selectSingleOption = function (optionName) {
      $('.ui-select-container').click();
      element(by.cssContainingText('.ui-select-choices-row', optionName)).click();
    };

    api.enterText = function (text) {
      $('div[ng-include="\'app/apps/list/fields/form/text-field.html\'"]').$('input[ng-model="model.value"]').sendKeys(text);
    };

    api.selectCheckbox = function (groupIndex) {
      var elem = $$('.form-group').get(groupIndex);
      var checkbox = new Checkbox(elem);
      checkbox.click();
    };
  }

  module.exports = ListEntry;

})();
