(function () {
  'use strict';

  var login = require('./../../../login.page.js');
  var list = require('./admin.group-list.page.js');
  var details = require('./admin.group-details.page.js');
  var components = require('./../../../components.page.js');

  describe('group administration', function () {

    beforeAll(function () {
      login.loginDefaultUser();

    });

    beforeEach(function () {
      list.get();
    });

    it('should sort the group list by name', function () {
      // descending
      list.table.headers.name.click();
      list.table.rows.names().then(function (values) {
        expect(values).toEqual(values.slice().sort(compareIgnoreCase).reverse());
      });

      // ascending
      list.table.headers.name.click();
      list.table.rows.names().then(function (values) {
        expect(values).toEqual(values.slice().sort(compareIgnoreCase));
      });
    });

    it('should create, update and delete a simple group', function () {
      var suffix = Math.floor(Math.random() * 1000000); // for unique test data
      var name = 'testgroup' + suffix;

      // create group
      list.createButton.click();
      expect(details.isSaveButtonDisabled()).toBe(true);

      details.name.sendKeys(name);
      expect(details.isSaveButtonDisabled()).toBe(false);
      details.saveButton.click();

      // validate created group
      verifyGroup(name);

      // update group name
      details.name.sendKeys('-updated');
      details.saveButton.click();

      // validate updated group
      verifyGroup(name + '-updated');

      // delete group
      details.cancelButton.click();
      openOptions().deleteOption.click();
      components.modals.confirm.deleteButton.click();
      expect(list.table.rows.count()).toBe(0);
    });

    it('should create, update and delete a group with a role', function () {
      var suffix = Math.floor(Math.random() * 1000000); // for unique test data
      var name = 'testgroup' + suffix;
      var roleName = 'Admin';
      var roleNameAlt = 'User';

      // create group
      list.createButton.click();
      expect(details.isSaveButtonDisabled()).toBe(true);

      details.name.sendKeys(name);
      expect(details.isSaveButtonDisabled()).toBe(false);
      details.roles.openDropdown();
      details.roles.selectOption(roleName);
      details.saveButton.click();

      // validate created group
      verifyGroup(name, [roleName]);

      // add user role
      details.roles.openDropdown();
      details.roles.selectOption(roleNameAlt);
      details.saveButton.click();

      // validate updated group
      verifyGroup(name, [roleName, roleNameAlt]);

      // delete group
      details.cancelButton.click();
      openOptions().deleteOption.click();
      components.modals.confirm.deleteButton.click();
      expect(list.table.rows.count()).toBe(0);
    });

    function openOptions() {
      var options = list.table.rows.get(0).options();
      options.open();
      return options;
    }

    function verifyGroup(expectedName, expectedRoles) {
      list.nameFilter.clear();
      list.nameFilter.sendKeys(expectedName);
      expect(list.table.rows.count()).toBe(1);
      var options = openOptions();
      options.editOption.click();

      expect(details.name.getAttribute('value')).toBe(expectedName);

      expectedRoles && expectedRoles.forEach(function (roleName) {
        expect(details.roles.selectedOptions()).toContain(roleName);
      });
    }

    function compareIgnoreCase(a, b) {
      return a.toLowerCase().localeCompare(b.toLowerCase());
    }
  });

})();
