import {PagesListPage} from '../../../model/page/pages/PagesListPage';
import {PageInterceptor} from '../../../model/PageInterceptor';

describe('pages', () => {

    beforeEach(() => {
        PageInterceptor.registerDefaultInterceptors();
    });

    afterEach(() => {
        // testhelper.deletePages();
    });

    it('open the pages list', () => {
        const pagesListPage = new PagesListPage();

        pagesListPage.open();

        expect(pagesListPage.isDisplayed()).toBeTruthy();
    });
});
