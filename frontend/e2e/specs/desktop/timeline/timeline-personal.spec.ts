import {PageInterceptor} from '../../../model/PageInterceptor';
import {HomePage} from '../../../model/page/HomePage';
import {TestHelper} from '../../../model/Util';

describe('personal timeline', () => {

    const homePage = new HomePage();

    beforeAll(() => {
        PageInterceptor.registerDefaultInterceptors();
    });

    beforeEach(() => {
        homePage.open();
    });

    it('should load items', () => {
        const personalTimeline = homePage.personalTimeline;

        expect(personalTimeline.isPresent()).toBeTruthy();
        expect(personalTimeline.items().count()).toBeGreaterThan(0);
    });

    it('should post a message', () => {
        const message = 'Some message ' + TestHelper.isoDateTime();
        const personalTimeline = homePage.personalTimeline;

        personalTimeline.postMessage(message);

        personalTimeline.expectNewestMessageToBe(message);
    });

    it('should like a post', () => {
        const message = 'A likable message ' + TestHelper.isoDateTime();
        const like = homePage.personalTimeline
            .postMessageAndExpect(message)
            .items().first()
            .like;

        like.set();
        expect(like.isChecked()).toBeTruthy();
        like.reset();
        expect(like.isChecked()).toBeFalsy();
        like.toggle();
        expect(like.isChecked()).toBeTruthy();
        like.toggle();
        expect(like.isChecked()).toBeFalsy();
    });

    it('should comment a post', () => {
        const timelineMessage = 'Some message with a comment ' + TestHelper.isoDateTime();
        const commentMessage = 'Some comment ' + TestHelper.isoDateTime();
        const comments = homePage.personalTimeline
            .postMessageAndExpect(timelineMessage)
            .items().first()
            .comments;

        comments.postComment(commentMessage);

        comments.expectNewestCommentToBe(commentMessage);
    });

    it('should edit a post', () => {
        const timelineMessage = 'I will be edited ' + TestHelper.isoDateTime();
        const newMessage = 'I was edited ' + TestHelper.isoDateTime();
        const timelineItem = homePage.personalTimeline
            .postMessageAndExpect(timelineMessage)
            .items().first();

        timelineItem.setEditMode().editDialog
            .fillForm({message: newMessage})
            .doSubmit();

        homePage.personalTimeline.expectNewestMessageToBe(newMessage);
        expect(timelineItem.wasEdited.isDisplayed).toBeTruthy();
    });
});
