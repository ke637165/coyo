(function () {
  'use strict';

  function WorkspaceCreate() {
    var api = this;

    var form1 = $('form[name="workspaceForm1"]');
    api.page1 = {
      name: element(by.model('$ctrl.workspace.name')),
      description: element(by.model('$ctrl.workspace.description')),
      continueButton: form1.$('.btn-primary'),
      cancelButton: form1.$('.btn-default')
    };

    var form2 = $('form[name="workspaceForm2"]');
    api.page2 = {
      continueButton: form2.$('.btn-primary'),
      cancelButton: form2.$('.btn-default')
    };
  }

  module.exports = WorkspaceCreate;

})();
