(function () {
  'use strict';

  var login = require('../../login.page.js');
  var Navigation = require('./../navigation.page.js');
  var Profile = require('./../profile/profile.page.js');
  var EventDetails = require('./../events/event-details.page.js');
  var EventList = require('./../events/event-list.page.js');
  var EventCreate = require('./../events/event-create.page.js');
  var component = require('../../components.page.js');
  var testhelper = require('../../../testhelper.js');

  describe('notification activity', function () {
    var navigation, profile;
    var username, password, firstname, lastname;
    var eventCreate, eventList, eventDetails;
    var usernameNancy, passwordNancy;

    beforeAll(function () {
      login.loginDefaultUser();
      var key = Math.floor(Math.random() * 1000000);
      username = 'max1.mustermann1.' + key + '@mindsmash.com';
      password = 'Secret123';
      firstname = 'Max1';
      lastname = 'Mustermann1' + key;
      usernameNancy = 'nf';
      passwordNancy = 'demo';
      testhelper.createUser(firstname, lastname, username, password, ['User']);
      navigation = new Navigation();
      profile = new Profile();
      eventCreate = new EventCreate();
      eventDetails = new EventDetails();
      eventList = new EventList();
    });

    afterAll(function () {
      testhelper.deleteUsers();
    });

    it('create notification for activity and read it', function () {
      // follow Max1 Mustermann1
      browser.get('/profile/' + firstname + '-' + lastname + '/activity');
      testhelper.cancelTour();
      profile.senderActions.follow.click();
      expect(profile.senderActions.followIsSuccess).toBeTruthy();

      login.logout();

      // login as Max1 Mustermann1
      login.login(username, password);
      testhelper.cancelTour();

      // check if a notification is pending
      expect(navigation.notification.isUnseen).toBeTruthy();

      navigation.notification.open();
      navigation.notification.activity.click();

      // check if notification is new
      navigation.notification.time(0).getText().then(function (text) {
        var bool = false;
        if (text === 'a few seconds ago' || text === 'a minute ago') {
          bool = true;
        }
        expect(bool).toBe(true);
      });

      // view notification
      navigation.notification.notifications.get(0).click();
      testhelper.cancelTour();
      expect(profile.title.getText()).toBe('Ian Bold');

      login.logout();
      login.loginDefaultUser();
    });

    it('redirect to current page when link of notification directs to a deleted event', function () {
      var key = Math.floor(Math.random() * 1000000);
      var eventName = 'createTestEvent' + key;
      // create event
      navigation.events.click();
      testhelper.cancelTour();

      eventList.newButton.click();
      eventCreate.page1.name.sendKeys(eventName);
      eventCreate.page1.continueButton.click();
      eventCreate.page2.continueButton.click();
      eventCreate.page3.continueButton.click();

      eventCreate.chooseUser.choose();
      var nancy = 'Nancy Fork';
      eventCreate.chooseUser.selectUserName(nancy);
      eventCreate.chooseUser.selectUserBtn.click();

      eventCreate.page4.continueButton.click();

      testhelper.cancelTour();
      expect(eventDetails.title.getText()).toBe(eventName);

      // delete event
      eventDetails.options.settings.click();
      eventDetails.settings.deleteButton.click();
      component.modals.confirm.deleteButton.click();

      login.logout();
      login.login(usernameNancy, passwordNancy);
      testhelper.cancelTour();

      navigation.notification.open();
      navigation.notification.activity.click();
      navigation.notification.time(0).click();

      expect(element(by.css('.state-main-landing-page-show'))).toBeDefined();
      expect(element(by.css('.welcome-widget'))).toBeDefined();
    });

  });

})();
