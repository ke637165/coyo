(function () {
  'use strict';

  var extend = require('util')._extend;

  function SubscriptionsWidget(widget) {
    var api = extend(this, widget);

    api.renderedWidget = {
      pages: $$('.subscriptions-widget-list-page li a[ng-click="::$ctrl.open(sender)"]')
    };
  }

  module.exports = SubscriptionsWidget;

})();
