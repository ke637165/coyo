(function () {
  'use strict';

  var extend = require('util')._extend;
  var Select = require('../../../select.page');

  function BlogWidget(widget) {
    var api = extend(this, widget);

    api.settings = {
      sourceSelection: {
        all: element(by.id('source-all')),
        subscribed: element(by.id('source-subscribed')),
        selected: element(by.id('source-selected'))
      },
      sources: new Select($('.ui-select-container.ui-select-multiple'), true),
      articleCount: element(by.model('model.settings._articleCount'))
    };

    api.view = {
      blogs: element.all(by.repeater('article in ::$ctrl.articles'))
    };
  }
  module.exports = BlogWidget;

})();
