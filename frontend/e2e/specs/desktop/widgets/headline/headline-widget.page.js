(function () {
  'use strict';

  var extend = require('util')._extend;
  function HeadlineWidget(widget) {
    var api = extend(this, widget);

    api.headline = widget.container.$('.headline-widget-headline');

    api.headlineHasClass = function (cls) {
      return widget.container.$('.headline-widget').getAttribute('class').then(function (classes) {
        return classes.split(' ').indexOf(cls) !== -1;
      });
    };

    api.inlineOptions = {
      sizeToggle: widget.container.$('.widget-options .zmdi-format-size'),
      sizeOption: function (label) {
        return widget.container.element(by.cssContainingText('.widget-options a', label));
      }
    };
  }
  module.exports = HeadlineWidget;
})();
