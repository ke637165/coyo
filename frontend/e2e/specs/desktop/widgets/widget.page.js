(function () {
  'use strict';

  function Widget(container) {
    var api = this;
    api.container = container;
    api.editButton = api.container.$('.widget-edit');
    api.removeButton = api.container.$('.widget-remove');
    api.hover = function () {
      browser.actions().mouseMove(api.container).perform();
    };
  }

  module.exports = Widget;

})();
