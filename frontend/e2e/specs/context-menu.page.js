(function () {
  'use strict';

  function ContextMenu(container) {
    var api = this;

    api.element = container.$('.context-menu');

    api.openAndSelectOption = function (label) {
      open();
      selectOption(label);
    };

    function open() {
      api.element.click();
    }

    function selectOption(label) {
      api.element.element(by.cssContainingText('ul li', label)).click();
    }
  }

  module.exports = ContextMenu;

})();
