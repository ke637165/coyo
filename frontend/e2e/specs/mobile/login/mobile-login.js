(function () {
  'use strict';

  var login = require('../../login.page.js');
  var MobileNavigation = require('../mobile-navigation.page');

  describe('mobile login', function () {
    var mobileNavigation;

    beforeEach(function () {
      mobileNavigation = new MobileNavigation();
    });

    it('login and logout admin user', function () {
      // login as admin user Ian
      login.login('ian.bold@coyo4.com', 'demo');
      // depending on the backend strategy the user will end up on different pages, for configurable: timeline, for static: search
      expect(browser.getCurrentUrl()).toMatch(browser.baseUrl + '/home/timeline|' + browser.baseUrl + '/search');

      // open navigation to logout
      mobileNavigation.naviagtion.open();
      expect(mobileNavigation.naviagtion.groupMore.elem.isPresent()).toBeTruthy();
      mobileNavigation.naviagtion.groupMore.open().then(function () {
        expect(mobileNavigation.naviagtion.groupMore.logoutButton.isPresent()).toBeTruthy();
        mobileNavigation.naviagtion.groupMore.logoutButton.click();

        // test if logout screen with message appears
        expect(login.logoutMessageHeader.isPresent()).toBeTruthy();
      });

    });
  });

})();
