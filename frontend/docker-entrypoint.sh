#!/bin/bash

set -e

rm /usr/local/apache2/htdocs/config.js

echo "var Config = { applicationName: '${COYO_APP_NAME}', backendUrlStrategy: '${COYO_BACKEND_URL_STRATEGY}', backendUrl: '${COYO_BACKEND_URL}', debug: false };" > /usr/local/apache2/htdocs/config.js

# Start httpd
exec httpd-foreground
