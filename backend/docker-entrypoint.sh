#!/bin/bash

set -e;

cat /etc/hosts | sed "s|localhost|localhost $(hostname)|g" > /etc/hosts

SKIP_WAIT_FOR_SERVICES="${SKIP_WAIT_FOR_SERVICES}"
if [ -z "${SKIP_WAIT_FOR_SERVICES}" ]; then
  /coyo/wait-for-it/wait-for-it.sh --timeout=120 "coyo-db:5432"
  /coyo/wait-for-it/wait-for-it.sh --timeout=120 "coyo-es:9300"
  /coyo/wait-for-it/wait-for-it.sh --timeout=120 "coyo-mq:5672"
  /coyo/wait-for-it/wait-for-it.sh --timeout=120 "coyo-stomp:61613"
  /coyo/wait-for-it/wait-for-it.sh --timeout=120 "coyo-redis:6379"
  /coyo/wait-for-it/wait-for-it.sh --timeout=120 "coyo-mongo:27017"
  /coyo/wait-for-it/wait-for-it.sh --timeout=120 "coyo-tika:9998"
fi

cd /coyo
bash -C '/coyo/start.sh';'bash'
